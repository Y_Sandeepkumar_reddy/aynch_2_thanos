/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
const lists = require("./lists_1.json");

const fs = require('fs');
function boardlist(boardID, callback) {
    setTimeout(() => {
        const boardLists = lists[boardID]; 
        if (boardLists) {
            console.log("Lists found for boardID:", boardID);
            // console.log("Lists:", boardLists);
            callback (boardLists)
        } else {
            console.log("No lists found for the specified board ID");
        }
    }, 2000);
}



module.exports = boardlist;

