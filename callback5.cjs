// /* 
// 	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

//     Get information from the Thanos boards
//     Get all the lists for the Thanos board
//     Get all cards for the Mind and Space lists simultaneously
// */
const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");
function thanosInfo() {
    // Step 1: Get information from the Thanos board
    getBoard("mcu453ed", board => {
        console.log("Information from the Thanos board:");
        console.log(board);

        // Step 2: Get all the lists for the Thanos board
        boardlist(board.id, lists => {
            console.log("All lists for the Thanos board:");
            console.log(lists);

            // Step 3: Get all cards for the Mind and Space lists
            const mindList = lists.find(list => list.name === "Mind");
            const spaceList = lists.find(list => list.name === "Space");

            if (!mindList || !spaceList) {
                console.error("Mind or Space list not found.");
                return;
            }

            // Fetch cards for the Mind list
            getCardsByListID(mindList.id, (error1, mindCards) => {
                if (error1) {
                    console.error("Error fetching cards for the Mind list:", error1);
                } else {
                    console.log("All cards for the Mind list:");
                    console.log(mindCards);
                }

                // Fetch cards for the Space list
                getCardsByListID(spaceList.id, (error2, spaceCards) => {
                    if (error2) {
                        console.error("Error fetching cards for the Space list:", error2);
                    } else {
                        console.log("All cards for the Space list:");
                        console.log(spaceCards);
                    }
                });
            });
        });
    });
}
// thanosInfo();
module.exports = thanosInfo;
