
const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function thanosInfo() {
    // Step 1: Get information from the Thanos board
    getBoard("mcu453ed", function(board) {
        console.log("Information from the Thanos board:");
        console.log(board);

        // Step 2: Get all the lists for the Thanos board
        boardlist(board.id, function(lists) {
            console.log("Lists found for boardID:", board.id);
            console.log("Lists:", lists);
            
            

            // Step 3: Get all cards for all lists
            lists.forEach(function(list) {
                // Fetch cards for the current list
                getCardsByListID(list.id, function(error, cards) {
                    if (error) {
                        console.error(`Error fetching cards for list ${list.id}:`, error);
                    } else {
                        console.log(`All cards for list ${list.name} (${list.id}):`);
                        console.log(cards);
                    }
                });
            });
        });
        
        
    });
}

// thanosInfo();
module.exports=thanosInfo;
