// Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json
// and then pass control back to the code that called it by using a callback function.



const boards = require("./boards_1.json");
function getBoard(boardID, callback) {
    setTimeout(() => {
        const board = boards.find(board => board.id === boardID);

        if (board) {
            callback(board); 
        } else {
            callback("Board not found", null);
        }
    }, 2000); 
}

module.exports = getBoard;




