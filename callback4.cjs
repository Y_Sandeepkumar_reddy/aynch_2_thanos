/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const getBoard = require("./callback1.cjs");
const boardlist = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function thanosInfo() {
    // Step 1: Get information from the Thanos board
    getBoard("mcu453ed", board => {
        console.log("Information from the Thanos board:");
        console.log(board);

        // Step 2: Get all the lists for the Thanos board
        boardlist(board.id, lists => {
            console.log("All lists for the Thanos board:");
            console.log(lists);

            // Step 3: Get all cards for the Mind list simultaneously
            const mindList = lists.find(list => list.name === "Mind");
            getCardsByListID(mindList.id, (error1, mindCards) => {
                if (error1) {
                    console.error("Error fetching cards for the Mind list:", error1);
                } else {
                    console.log("All cards for the Mind list:");
                    console.log(mindCards);
                }
            });
            
        });
    });
}

// getThanosBoardInfo();

module.exports = thanosInfo;