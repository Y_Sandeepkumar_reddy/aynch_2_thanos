const getCardsByListID = require("../callback3.cjs");

getCardsByListID("ghnb768", (error, cards) => {
    if (error) {
        console.error(error);
    } else {
        console.log("Cards belonging to the list:", cards);
    }
});