/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. 
   Then pass control back to the code that called it by using a callback function.
*/


const cardsData = require("./cards_1.json");


function getCardsByListID(listID, callback) {
    setTimeout(() => {
        const cards = cardsData[listID]; 
        
        if (cards && Array.isArray(cards)) {
            callback(null, cards); 
        } else {
            callback("No cards found for the specified list ID", null);         }
    }, 2000); 
}


module.exports = getCardsByListID;
